## Running server

`./gradlew bootRun`

## Running test
`./gradlew test`

## Lombok support Eclipse
`./gradlew installLombok`

## Packaging application
`./gradlew bootRepackage`
