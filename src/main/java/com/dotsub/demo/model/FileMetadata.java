package com.dotsub.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/12/16.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileMetadata implements Serializable {
    private static final long serialVersionUID = -556198372489298609L;
    private Long id;
    @NonNull
    private String title;
    @NonNull
    private String description;
    @NonNull
    private LocalDateTime creationDate;

    public FileMetadata() {
    }

    public FileMetadata(final String title, final String description, final LocalDateTime creationDate) {
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
    }
}
