package com.dotsub.demo.util;

import com.dotsub.demo.domain.FileMetadataEntity;
import com.dotsub.demo.exception.StorageException;
import com.dotsub.demo.model.FileMetadata;
import lombok.val;
import org.apache.tika.Tika;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/14/16.
 */
public class MetadataTranslateUtils {
    public static FileMetadata translateFromEntity(final FileMetadataEntity entity) {
        return new FileMetadata(
                entity.getTitle(),
                entity.getDescription(),
                entity.getCreationDate()
        );
    }

    public static FileMetadataEntity tranlateToEntity(final MultipartFile file) {
        val creationDateKey = "creationDate";
        val titleKey = "title";
        val descriptionKey = "description";

        try {
            val inputStream = file.getInputStream();
            val filename = file.getOriginalFilename();
            val metadata = MetadataUtils.extractMetadata(inputStream, filename);

            val title = !StringUtils.isEmpty(metadata.get(titleKey))
                    ? metadata.get(titleKey)
                    : filename;
            val description = !StringUtils.isEmpty(metadata.get(descriptionKey))
                    ? metadata.get(descriptionKey)
                    : "This is a(n) " + new Tika().detect(inputStream, filename) + " file.";

            return new FileMetadataEntity(
                    title,
                    description,
                    MetadataUtils.extractCreationDate(metadata.get(creationDateKey), file)
            );

        } catch (final IOException e) {
            throw new StorageException("Couldn't translate entity", e);
        }
    }
}
